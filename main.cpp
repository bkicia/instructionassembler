//Brianna Kicia   bkicia@vt.edu
//ECE 2500 Project 1: Assembler

#include <iostream>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QString>

//Global lists that can be accessed by all functions
QStringList RType = (QStringList() << "add" <<"addu" << "and" << "jr" << "nor" << "or" << "slt" << "sltu" << "sll" << "srl" << "sub" << "subu");
QStringList RFunct = (QStringList() << "20" << "21"  << "24"  << "08" << "27"  << "25" << "2a"  << "2b"   << "00"  << "02"  << "22"  << "23");

QStringList IType = (QStringList() << "addi" << "addiu" << "andi" << "ori" << "slti" << "sltiu" << "lbu" << "lhu" << "ll" << "lw" << "sb" << "sc" << "sh" << "sw" << "lui" << "beq" << "bne");
QStringList IOpcode = (QStringList() << "8"   << "9"     <<  "c"   << "d"   << "a"    << "b"     << "24"  << "25"  << "30" << "23" << "28" << "38" << "29" << "2b" << "f"   << "4"   << "5");

QStringList RegName = (QStringList() << "$zero" << "$at" << "$v0" << "$v1" << "$a0" << "$a1" << "$a2" << "$a3" << "$t0" << "$t1" << "$t2" << "$t3" << "$t4" << "$t5" << "$t6" << "$t7" << "$s0" << "$s1" << "$s2" << "$s3" << "$s4" << "$s5" << "$s6" << "$s7" << "$t8" << "$t9" << "$k0" << "$k1" << "$gp" << "$sp" << "$fp" << "$ra");
QStringList RegNumber = (QStringList() << "0"   << "1"   << "2"   << "3"   << "4"   << "5"   << "6"   << "7"   << "8"   << "9"   << "10"  << "11"  << "12"  << "13"  << "14"  << "15"  << "16"  << "17"  << "18"  << "19"  << "20"  << "21"  << "22"  << "23"  << "24"  << "25"  << "26"  << "27"  << "28"  << "29"  << "30"  << "31");

//Global lists to hold labels and instructions
QStringList labelsFound = QStringList();
QStringList labelsRequested = QStringList();
QStringList instructions = QStringList();

bool f = false;

QString splitInputLine(QString input, int lineNumber);
QString instructionType(QString name);
QString handleIType(QStringList line, int lineNumber);
QString handleRType(QStringList line);
QString findBinaryEquivalent(QString toMatch, QStringList findMatch, QStringList matchValue, int &counter, int capSize, int base);
bool handleLabelFound(QString label, int lineNumber);
void handleLabelRequested(QString label, int lineNumber);

int main (int argc, char* argv[])
{
	//if there was an incorrect number of inputs from the cmd
	if (argc != 2) {
    	std::cerr << "Error: a *.asm file is needed" << std::endl;
    	return 1;
  	}

  	//finds the correct files with the correct extensions
  	QString inputFileName = argv[1];
  	QString outputFileName = argv[1];
  	inputFileName.append(".asm");
  	outputFileName.append(".txt");
	QFile ASMFile(inputFileName);
	QFile TXTFile(outputFileName);

	//makes sure the file *.asm is readable and *.txt is writable
	if (!ASMFile.open(QIODevice::ReadOnly))
		std::cerr << "Unable to read from: " << inputFileName.toStdString() << std::endl;
	if (!TXTFile.open(QIODevice::WriteOnly))
		std::cerr << "Unable to write to: " << outputFileName.toStdString() << std::endl;
	
	QTextStream in(&ASMFile);
	int counter = 1;

	//loop looking at each line of the code individually
	while(!in.atEnd())	{
		QString line = in.readLine();
		QString binary = splitInputLine(line, counter);
		//if a binary instruction is returned, convert it to hex and make sure it is 8 digits long
		if (binary != NULL)	{
			QString hex = QString::number(binary.toLongLong(&f, 2), 16);
			while (hex.length() != 8)
				hex.prepend('0');
			instructions.append(hex);
		}
		//if a NULL instruction is returned, that means an error was found at that line
		else	{
			std::cerr << "Cannot assemble the assembly code at line " << counter << "\n";
			return 0;
		}
		counter += 1;
	}

	//loop for completing bne/beq instructions 
	while(!labelsRequested.isEmpty())	{
		//get the requested label and line number
		int requestedLineNumber = labelsRequested.at(0).toInt();
		labelsRequested.removeAt(0);
		QString requestedLabel = labelsRequested.at(0);
		labelsRequested.removeAt(0);

		//check if the label is found and if not print an error message
		int counter = labelsFound.indexOf(requestedLabel);
		if (counter == -1)	{
			std::cerr << "Undefined label " << requestedLabel.toStdString() << " at line " << requestedLineNumber-1 << std::endl;
			return 0;
		}

		//calculate the immediate from the line numbers
		int foundLineNumber = labelsFound.at(counter-1).toInt();
		QString labelImmediate = QString::number(foundLineNumber-requestedLineNumber);
		labelImmediate = QString::number(labelImmediate.toLongLong(&f, 10), 16);
		//make sure the immediate is the correct length
		while (labelImmediate.length() > 4)
			labelImmediate.remove(0,1);
		if (foundLineNumber-requestedLineNumber >= 0)
			while (labelImmediate.length() != 4)
				labelImmediate.prepend('0');
		else
			while (labelImmediate.length() != 4)
				labelImmediate.prepend('F');
		//remove the existing immediate (4'h0) and add the correct immediate
		QString newInstruction = instructions.at(requestedLineNumber-2);
		newInstruction.truncate(4);
		newInstruction.append(labelImmediate);
		instructions.replace(requestedLineNumber-2, newInstruction);
	}

	QTextStream out(&TXTFile);
	//print the correct instructions to the *.txt file
	for (int i = 0; i < instructions.length(); i ++)
		out << instructions.at(i) << "\n";

	ASMFile.close();
	TXTFile.close();
	return 0;
}

//Function for taking the entire line from the *.asm file and seperating it based on
//label, name, arguments, comments
QString splitInputLine(QString input, int lineNumber)	{
	input.remove(',');
	input.remove('\t');
	QStringList seperateInput = (QStringList() << NULL << NULL << NULL << NULL << NULL << NULL);
	seperateInput = input.split(" ");
	QStringList labelNameArgs;

	if (seperateInput.at(0).contains("#") || seperateInput.size() < 2)
		return NULL;

	int counter = 0;

	if (seperateInput.at(0).endsWith(":"))	{
		QString label = seperateInput.at(0);
		seperateInput.removeAt(0);
		label.remove(':');
		handleLabelFound(label, lineNumber);
		counter += 1;
	}

	for (int i = 0; i < seperateInput.size(); i++)	{
		if (seperateInput.at(i).contains('#'))	{
			QString removeComment = seperateInput.at(i);
			int index = removeComment.indexOf('#');
			removeComment.truncate(index);
			labelNameArgs.append(removeComment);
		}
		else
			labelNameArgs.append(seperateInput.at(i));
	}
	QString type = instructionType(labelNameArgs.at(0));
	if (type == "RType")
		return handleRType(labelNameArgs);
	else if (type == "IType")
		return handleIType(labelNameArgs, lineNumber);

	return NULL;
}

//Function for determing if the name is an R-Type or I-Type instruction
QString instructionType(QString name)	{
	if (RType.contains(name))
		return "RType";
	else if (IType.contains(name))
		return "IType";
	else
		return NULL;
}

//Function for generating the I-Type binary instruction representation
QString handleIType(QStringList line, int lineNumber)	{
	QString instruction;
	int typeCounter = 0, registerCounter = 0;

	QString binaryFunct = findBinaryEquivalent(line.at(0), IType, IOpcode, typeCounter, 6, 16);
	QString binaryRS = "00000", binaryRT = "00000", binaryImm = "0000000000000000";
	int base = 10;
	if (typeCounter <= 14)	{
		binaryRT = findBinaryEquivalent(line.at(1), RegName, RegNumber, registerCounter, 5, 10);

		registerCounter = 0;
		if (typeCounter == 14)
			binaryImm = QString::number(line.at(2).toLongLong(&f, 10), 2);
		else if (typeCounter <= 5)	{
			binaryRS = findBinaryEquivalent(line.at(2), RegName, RegNumber, registerCounter, 5, 10);
			if (line.at(3).contains('x'))
				base = 16;
			binaryImm = QString::number(line.at(3).toLongLong(&f, base), 2);
		}
		else {
			QStringList offsetAndReg = line.at(2).split('(');
			QString reg = offsetAndReg.at(1);
			binaryRS = findBinaryEquivalent(reg.remove(')'), RegName, RegNumber, registerCounter, 5, 10);
			if (offsetAndReg.at(0).contains('x'))
				base = 16;
			binaryImm = QString::number(offsetAndReg.at(0).toLongLong(&f, base), 2);
		}
	}
	else	{
		binaryRS = findBinaryEquivalent(line.at(1), RegName, RegNumber, registerCounter, 5, 10);
		binaryRT = findBinaryEquivalent(line.at(2), RegName, RegNumber, registerCounter, 5, 10);
		handleLabelRequested(line.at(3), lineNumber);
	}
	while (binaryImm.length() != 16)
				binaryImm.prepend('0');

	instruction.append(binaryFunct);
	instruction.append(binaryRS);
	instruction.append(binaryRT);
	instruction.append(binaryImm);
	return instruction;
}

//Function for generating the R-Type binary instruction representation
QString handleRType(QStringList line)	{
	QString instruction;

	int typeCounter = -1, registerCounter = -1;

	QString binaryFunct = findBinaryEquivalent(line.at(0), RType, RFunct, typeCounter, 6, 16);
	QString binaryRD = "00000", binaryRS = "00000", binaryRT = "00000", binaryShamt = "00000";
	
	if (typeCounter != 3 && typeCounter !=-1)	{
		binaryRD = findBinaryEquivalent(line.at(1), RegName, RegNumber, registerCounter, 5, 10);

		registerCounter = 0;
		
		if (typeCounter != 8 && typeCounter != 9) {
			binaryRS = findBinaryEquivalent(line.at(2), RegName, RegNumber, registerCounter, 5, 10);
			registerCounter = 0;
			binaryRT = findBinaryEquivalent(line.at(3), RegName, RegNumber, registerCounter, 5, 10);
		}
		else {
			binaryRT = findBinaryEquivalent(line.at(2), RegName, RegNumber, registerCounter, 5, 10);
			binaryShamt = QString::number(line.at(3).toLongLong(&f, 10), 2);
			if (binaryShamt == "0" && line.at(3) != "0")
				binaryShamt = QString();
		}
	}
	else if (typeCounter == 3)
		binaryRS = findBinaryEquivalent(line.at(1), RegName, RegNumber, registerCounter, 5, 10);
	if (binaryFunct == NULL || binaryRD == NULL || binaryRS == NULL || binaryRT == NULL || binaryShamt == NULL)
		return NULL;

	while (binaryShamt.length() != 5)
				binaryShamt.prepend('0');

	instruction.append("000000");
	instruction.append(binaryRS);
	instruction.append(binaryRT);
	instruction.append(binaryRD);
	instruction.append(binaryShamt);
	instruction.append(binaryFunct);
	return instruction;
}

//Function for getting the binary equivalent of the registers or names (listed in global QStringLists)
QString findBinaryEquivalent(QString toMatch, QStringList findMatch, QStringList matchValue, int &counter, int capSize, int base)	{
	if (toMatch == NULL)
		return NULL;
	counter = findMatch.indexOf(toMatch);
	if (counter != -1)	{
		QString binaryFunct = QString::number(matchValue.at(counter).toLongLong(&f, base), 2);
		while (binaryFunct.length() != capSize)
			binaryFunct.prepend('0');
		return binaryFunct;
	}
	return NULL;
}

//Function that handles when an instruction has a label in front
bool handleLabelFound(QString label, int lineNumber) {
	if (labelsFound.contains(label))
		return false;
	labelsFound.append(QString::number(lineNumber));
	labelsFound.append(label);
	return true;
}

//Function that handles when an instruction calls a label (beq or bne)
void handleLabelRequested(QString label, int lineNumber)	{
	lineNumber += 1;
	labelsRequested.append(QString::number(lineNumber));
	labelsRequested.append(label);
}